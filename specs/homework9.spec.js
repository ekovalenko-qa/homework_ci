import {test} from '@jest/globals';
import {apiProvider} from '../framework';
import {tokens} from '../framework/config';

/** Тесты для проверки сервиса https://mailboxlayer.com/*/

test('1. Позитивный тест для: для верного email и key возвращает имя пользователя', async () => {
    const email = tokens.apilayer_email;
    const key = tokens.apilayer_key;
    const r = await apiProvider()
        .infoApiLayer()
        .get(key, email);
    expect(r.body.user)
        .toBe('katia.kovalenko');
});

test.each`
      key                                | email                         | expected
 ${'d3964f518ded2a7f3b11ebdf53add'}      | ${'katia.kovalenko@mail.ru'}  | ${'invalid_access_key'}
 ${''}                                   | ${'katia.kovalenko@mail.ru'}  | ${'missing_access_key'}
 ${'d3964f518ded2a7f3b11ebdf53add243'}   | ${''}                         | ${'no_email_address_supplied'}
  `('2. Параметризированный негативный тест (проверки ошибок в ответе)', async ({
                                                                                                                                                                                                                                                                                                                                                                                                           email,
                                                                                                                                                                                                                                                                                                                                                                                                           key,
                                                                                                                                                                                                                                                                                                                                                                                                           expected
                                                                                                                                                                                                                                                                                                                                                                                                       }) => {
    const r = await apiProvider()
        .infoApiLayer()
        .get(key, email);
    expect(r.body.error.type)
        .toBe(expected);
});

test('3. Проверяет права доступа к точке без api_key', async () => {
    const email = tokens.apilayer_email;
    const r = await apiProvider()
        .infoApiLayer()
        .get('', email);
    expect(r.body.error.type)
        .toBe('missing_access_key');
});

test.each`
      email                      | obj                         | expected
 ${'katia.kovalenko@mail.ru'}    | ${'format_valid'}           | ${true}
 ${'katia'}                      | ${'format_valid'}           | ${false}
 ${'katia@'}                     | ${'format_valid'}           | ${false}
 ${'@mail.ru'}                   | ${'format_valid'}           | ${false}
 ${'ekovalenko@primeaspect.ru'}  | ${'free'}                   | ${false}
 ${'support@apilayer.com'}       | ${'role'}                   | ${true}
  `('4. Параметризированный позитивный тест: проверки ответа', async ({
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        email,
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        obj,
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        expected
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    }) => {
    const r = await apiProvider()
        .infoApiLayer()
        .get(tokens.apilayer_key, email);
    expect(r.body[`${obj}`])
        .toBe(expected);
});

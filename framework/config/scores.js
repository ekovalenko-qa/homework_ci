const scores = {
  Anna: 10,
  Olga: 1,
  Ivan: 5,
};
const scores1 = {
  Anna: 10,
  Olga: 1,
  Ivan: 'Ivan',
};
const scores2 = {
  Anna: 10,
  Olga: 1,
  Ivan: -1,
};
const scores3 = {
  Anna: 10,
  Olga: 1,
  Ivan: true,
};
const scores4 = {};
const scores5 = {
  Anna: 10,
  Olga: 1,
  Ivan: Infinity,
};
const scores6 = {
  Anna: 10,
  Olga: -Infinity,
};
const scores7 = {
  Anna: 10.53,
  Olga: 1,
  Ivan: 5,
};
const scores8 = {
  Anna: [1, 2, 3],
  Olga: 1,
  Ivan: 5,
};
const scores9 = {
  Anna: 'December 17, 1995 03:24:00',
  Olga: 1,
  Ivan: 5,
};
const scores10 = {
  Anna: 10,
  Olga: 11,
  Ivan: 5,
};
export {
  scores,
  scores1,
  scores2,
  scores3,
  scores4,
  scores5,
  scores6,
  scores7,
  scores8,
  scores9,
  scores10,
};

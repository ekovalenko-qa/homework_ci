import supertest from 'supertest';
import {decorateService} from '../../lib/decorate';
import {urls} from '../config';

const InfoApiLayer = function InfoApiLayer() {
    this.get = async function InfoApiLayer(key, email) {
        const r = await supertest(urls.apilayer)
            .get(`/api/check?access_key=${key}&email=${email}`)
            .send();
        return r;
    };
};

decorateService(InfoApiLayer);
export {InfoApiLayer};

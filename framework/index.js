import { InfoApiLayer } from './services/index';

const apiProvider = () => ({
    infoApiLayer: () => new InfoApiLayer(),
});

export {apiProvider};
